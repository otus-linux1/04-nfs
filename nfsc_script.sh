yum -y install nfs-utils

systemctl start rpcbind 
systemctl enable rpcbind

mkdir /uploads

cat <<EOF > /etc/systemd/system/uploads.mount
[Unit]
Description=NFS share
[Mount]
What=192.168.50.10:/uploads
Where=/uploads
Type=nfs
Options=vers=3,proto=udp,rw,_netdev
DirectoryMode=0755
EOF

cat <<EOF > /etc/systemd/system/uploads.automount
[Unit]
Description=NFS share
Requires=network-online.target
[Automount]
Where=/uploads
[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl start uploads.automount
systemctl enable uploads.automount
