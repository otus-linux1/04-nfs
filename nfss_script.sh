
yum -y install nfs-utils
mkdir -p /uploads
chown nfsnobody:nfsnobody /uploads
echo '/uploads 192.168.50.0/24(rw,all_squash,anonuid=65534,anongid=65534)'>>/etc/exports

cat <<EOF > /etc/firewalld/services/nfs.xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>NFS</short>
  <description>NFS service</description>
  <port protocol="tcp" port="111"/>
  <port protocol="udp" port="111"/>
  <port protocol="tcp" port="2049"/>
  <port protocol="udp" port="2049"/>
  <port protocol="tcp" port="20048"/>
  <port protocol="udp" port="20048"/>
  <port protocol="tcp" port="32803"/>
  <port protocol="udp" port="32803"/>
  <port protocol="tcp" port="38467"/>
  <port protocol="udp" port="38467"/>
  <port protocol="tcp" port="32769"/>
  <port protocol="udp" port="32769"/>
</service>
EOF

systemctl enable firewalld
systemctl start firewalld
firewall-cmd --add-service=nfs --permanent
firewall-cmd --reload

for i in rpcbind nfs-server; do
    systemctl start $i
    systemctl enable $i
done
