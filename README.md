# NFS

```
vagrant up должен поднимать 2 виртуалки: сервер и клиент
на сервер должна быть расшарена директория
на клиента она должна автоматически монтироваться при старте (fstab или autofs)
в шаре должна быть папка upload с правами на запись
- требования для NFS: NFSv3 по UDP, включенный firewall
```

## Сервер

Файл `nfss_script.sh`

Сконфигурированы экспорт директории `/uploads` и запущен сервер nfs, настроен firewall 

## Клиент

Файл `nfsc_script.sh`

Настроено автомонтирование nfs-шары в `/uploads` через systemd

## Использованные дополнительные материалы

- https://computingforgeeks.com/configure-nfsv3-and-nfsv4-on-centos-7/
- https://cloudnull.io/2017/05/nfs-mount-via-systemd/
- https://habr.com/ru/post/331240/
